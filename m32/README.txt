Gunter Liszewski (S8)


Table of Contents
─────────────────

1. The Machine 'm32'
.. 1. We use Orgmode, again


1 The Machine 'm32'
═══════════════════

  This is new.  A 'KVM' machine.  In the net.

  We use 'btrfs subvolumes' to organise our storage.

  We start with subvolume 'boot0'.  This is about a bootable kernel for
  this machine.  It is about the 'bootloader', 'Grub' most likely.

  This is also about access to the node called 'm32' on the net.


1.1 We use Orgmode, again
─────────────────────────

  We start by reminding ourselves of the Emacs Orgmode facilities.  Key
  sequences and Elisp functions.

  ┌────
  │ C-c C-v t, C-c C-v C-t.
  │ 
  │ (org-babel-tangle &optional ARG TARGET-FILE LANG) 
  └────
